root=/workspace/otp-install
sudo rm -rf /usr/local/lib/erlang
sudo tar xvf $root/otp-25/otp-25-0-4.tar.xz -C /usr
cp -r $root/otp-25/.cache $HOME
sudo rm -rf /usr/local/lib/elixir
sudo tar xvf $root/elixir-1-13-4.tar.xz -C /usr
sudo cp $root/rebar3 /usr/local/bin
